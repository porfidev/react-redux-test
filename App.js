import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  useColorScheme,
} from 'react-native';
import {Provider} from 'react-redux';
import ExampleConnected from './components/Example.js';
import UpdateUserForm from './components/UpdateUserForm.js';
import UserExample from './components/UserExample.js';
import {store} from './store/store.js';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  return (
    <Provider store={store}>
      <SafeAreaView>
        <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
        <ScrollView contentInsetAdjustmentBehavior="automatic"></ScrollView>
        <ExampleConnected />
        <UserExample />
        <UpdateUserForm />
      </SafeAreaView>
    </Provider>
  );
};

export default App;
