const initialState = {
  lights: 'NONE',
  user: {
    userName: 'Churrumin',
    age: 99,
  },
};

export {initialState};
