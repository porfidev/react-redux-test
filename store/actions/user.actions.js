import {SET_USER_AGE, SET_USER_NAME} from './types.js';

const setUserName = userName => ({
  type: SET_USER_NAME,
  payload: {
    // opcional
    userName,
  },
});

const setUserAge = age => ({
  type: SET_USER_AGE,
  payload: {
    age,
  },
});

export {setUserName, setUserAge};
