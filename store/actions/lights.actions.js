import {RESET_LIGHT, SET_LIGHT} from './types.js';

const setLight = () => ({
  type: SET_LIGHT,
});

const resetLight = () => ({
  type: RESET_LIGHT,
});

export {setLight, resetLight};
