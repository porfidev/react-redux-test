import {combineReducers} from 'redux';
import {lightsReducer} from './lights.reducer.js';
import {userReducer} from './users.reducer.js';

const rootReducers = combineReducers({
  lights: lightsReducer,
  user: userReducer,
});

export {rootReducers};
