import {RESET_LIGHT, SET_LIGHT} from '../actions/types.js';
import {initialState} from '../initialState.js';

const lightsReducer = (state = initialState.lights, action) => {
  switch (action.type) {
    case SET_LIGHT:
      return 'TEST SET';
    case RESET_LIGHT:
      return '';
    default:
      return state;
  }
};

export {lightsReducer};
