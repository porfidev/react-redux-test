import {SET_USER_AGE, SET_USER_NAME} from '../actions/types.js';
import {initialState} from '../initialState.js';

const userReducer = (state = initialState.user, action) => {
  if (action.type === SET_USER_NAME) {
    return {...state, userName: action.payload.userName};
  }

  if (action.type === SET_USER_AGE) {
    return {...state, age: action.payload.age};
  }

  return state;
};

export {userReducer};
