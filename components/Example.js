import React from 'react';
import {Button, Text, View} from 'react-native';
import {connect} from 'react-redux';
import {setLight, resetLight} from '../store/actions/lights.actions.js';

const Example = ({lights, changeAction, resetAction}) => {
  return (
    <View style={{padding: 20}}>
      <Text>{lights}</Text>
      <Button title="Update" onPress={changeAction} />
      <Button title="Reset" onPress={resetAction} />
    </View>
  );
};

const mapStateToProps = state => {
  return {
    lights: state.lights,
  };
};

const mapDispatchToProps = {
  changeAction: setLight,
  resetAction: resetLight,
};

const ExampleConnected = connect(mapStateToProps, mapDispatchToProps)(Example);

export default ExampleConnected;
