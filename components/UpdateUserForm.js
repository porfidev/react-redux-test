import {useState} from 'react';
import {Button, Text, TextInput, View} from 'react-native';
import {connect} from 'react-redux';
import {setUserAge, setUserName} from '../store/actions/user.actions.js';

const UpdateUserForm = ({
  setUserNameAction,
  setUserAgeAction,
  userName,
  age,
}) => {
  const [user, setUser] = useState({
    name: userName,
    age: age,
  });

  const handleChange = (type, value) => {
    setUser({
      ...user,
      [type]: value,
    });
  };

  const handleUpdate = () => {
    setUserNameAction(user.name);
    setUserAgeAction(user.age);
  };

  return (
    <View>
      <Text>Nombre</Text>
      <TextInput
        value={user.name}
        onChangeText={text => handleChange('name', text)}
      />
      <Text>Edad</Text>
      <TextInput
        value={user.age.toString()}
        onChangeText={text => handleChange('age', text)}
      />
      <Button title="Actualizar" onPress={handleUpdate} />
    </View>
  );
};

const mapStateToProps = state => {
  return {
    userName: state.user.userName,
    age: state.user.age,
  };
};

const mapDispatchToProps = {
  setUserNameAction: setUserName,
  setUserAgeAction: setUserAge,
};

const UpdateUserFormConnected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UpdateUserForm);

export default UpdateUserFormConnected;
