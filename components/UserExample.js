import React from 'react';
import {Text, View} from 'react-native';
import {connect} from 'react-redux';

const UserExample = ({userName, age}) => {
  return (
    <View>
      <Text>Nombre: {userName}</Text>
      <Text>Edad: {age}</Text>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    userName: state.user.userName,
    age: state.user.age,
  };
};

const UserExampleConnected = connect(mapStateToProps, null)(UserExample);

export default UserExampleConnected;
